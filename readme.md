# fume extractor platform

fume extractor that goes under circuit board, so it actually sucks fumes before they get to your face.

# electrical parts

* [x] motor driver ic
    * under half-bridge, not motor drivers/controllers
    * https://www.digikey.com/products/en/integrated-circuits-ics/pmic-full-half-bridge-drivers/746?FV=-8%7C746%2C7%7C1%2C7%7C17%2C7%7C2%2C7%7C3%2C7%7C6&quantity=1&ColumnSort=1000011&page=1&stock=1&pageSize=25
* [x] fans
    * 4x fans arrangement
    * min total 105 CFM
* [ ] hmi & display
    * hmi
        * potentiometer
            * knob
            * wheel
        * pushbuttons
    * display
        * [x] leds 7-seg (single)
        * [ ] leds firefly display
        * [ ] leds discrete
    * power switch
    * [ ] research consumer models on amazon
* [x] 12V psu
    * [x] checked if current supply high enough
        * 1.0A >> 0.138 * 4 = 0.552 A (accounting for current spikes
        * supply is 12V @ 1A
* [x] connector (pair)
    * [x] ~~female barrel jack that matches one attached to psu (assuming connector is there)~~
* [ ] mcu
    * onhand (attiny45?)

# mechanicals

* [ ] frame
    * frame should accomodate for different sizes of filters; still in testing phase
* [ ] mesh
    * platform for pcb
        * not necessarily holder for pcb
* [?] filter material
    * carbon-based
    * not sure if existing is effective enough
        * [x] use existing material and see what happens
        * [ ] buy new material
