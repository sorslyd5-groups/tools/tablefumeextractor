EESchema Schematic File Version 4
LIBS:fan4-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 5E0F1B1B
P 5250 4850
F 0 "#PWR0101" H 5250 4600 50  0001 C CNN
F 1 "GND" H 5255 4677 50  0000 C CNN
F 2 "" H 5250 4850 50  0001 C CNN
F 3 "" H 5250 4850 50  0001 C CNN
	1    5250 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4750 5250 4850
Wire Wire Line
	5250 3550 5250 3450
$Comp
L power:+5V #PWR0104
U 1 1 5E102F44
P 5700 1700
F 0 "#PWR0104" H 5700 1550 50  0001 C CNN
F 1 "+5V" V 5700 1850 50  0000 L CNN
F 2 "" H 5700 1700 50  0001 C CNN
F 3 "" H 5700 1700 50  0001 C CNN
	1    5700 1700
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5E102FB8
P 5250 3450
F 0 "#PWR0105" H 5250 3300 50  0001 C CNN
F 1 "+5V" H 5265 3623 50  0000 C CNN
F 2 "" H 5250 3450 50  0001 C CNN
F 3 "" H 5250 3450 50  0001 C CNN
	1    5250 3450
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L78L05_SO8 U2
U 1 1 5E113F87
P 5200 1700
F 0 "U2" H 5200 1942 50  0000 C CNN
F 1 "L78L05_SO8" H 5200 1851 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5300 1900 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 5400 1700 50  0001 C CNN
	1    5200 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5E116755
P 5200 2200
F 0 "#PWR0106" H 5200 1950 50  0001 C CNN
F 1 "GND" H 5205 2027 50  0000 C CNN
F 2 "" H 5200 2200 50  0001 C CNN
F 3 "" H 5200 2200 50  0001 C CNN
	1    5200 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E117170
P 4800 1950
F 0 "C1" H 4550 2000 50  0000 L CNN
F 1 "1u" H 4550 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4838 1800 50  0001 C CNN
F 3 "~" H 4800 1950 50  0001 C CNN
	1    4800 1950
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0107
U 1 1 5E102E82
P 4700 1700
F 0 "#PWR0107" H 4700 1550 50  0001 C CNN
F 1 "+12V" V 4700 1950 50  0000 C CNN
F 2 "" H 4700 1700 50  0001 C CNN
F 3 "" H 4700 1700 50  0001 C CNN
	1    4700 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 1700 4800 1700
Connection ~ 4800 1700
Wire Wire Line
	4800 1700 4900 1700
$Comp
L Device:C C2
U 1 1 5E1192D8
P 5600 1950
F 0 "C2" H 5715 1996 50  0000 L CNN
F 1 "0.1u" H 5715 1905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5638 1800 50  0001 C CNN
F 3 "~" H 5600 1950 50  0001 C CNN
	1    5600 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1800 4800 1700
Wire Wire Line
	4800 2100 5200 2100
Wire Wire Line
	5200 2000 5200 2100
Connection ~ 5200 2100
Wire Wire Line
	5600 1800 5600 1700
Wire Wire Line
	5600 1700 5500 1700
Wire Wire Line
	5700 1700 5600 1700
Connection ~ 5600 1700
Wire Wire Line
	5200 2100 5600 2100
Wire Wire Line
	5200 2100 5200 2200
$Comp
L power:+12V #PWR0103
U 1 1 5E0EF9CB
P 3400 1700
F 0 "#PWR0103" H 3400 1550 50  0001 C CNN
F 1 "+12V" V 3415 1828 50  0000 L CNN
F 2 "" H 3400 1700 50  0001 C CNN
F 3 "" H 3400 1700 50  0001 C CNN
	1    3400 1700
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E0F0579
P 3000 1900
F 0 "#PWR0102" H 3000 1650 50  0001 C CNN
F 1 "GND" V 3005 1772 50  0000 R CNN
F 2 "" H 3000 1900 50  0001 C CNN
F 3 "" H 3000 1900 50  0001 C CNN
	1    3000 1900
	0    -1   1    0   
$EndComp
Wire Wire Line
	2900 1900 2950 1900
Wire Wire Line
	3000 1700 2900 1700
$Comp
L Device:LED D1
U 1 1 5E124555
P 2600 3400
F 0 "D1" H 2593 3145 50  0000 C CNN
F 1 "LED" H 2593 3236 50  0000 C CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 2600 3400 50  0001 C CNN
F 3 "~" H 2600 3400 50  0001 C CNN
	1    2600 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5E1254E7
P 2300 3400
F 0 "R1" V 2093 3400 50  0000 C CNN
F 1 "270" V 2184 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2230 3400 50  0001 C CNN
F 3 "~" H 2300 3400 50  0001 C CNN
	1    2300 3400
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5E129A2C
P 2600 3800
F 0 "D2" H 2593 3545 50  0000 C CNN
F 1 "LED" H 2593 3636 50  0000 C CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 2600 3800 50  0001 C CNN
F 3 "~" H 2600 3800 50  0001 C CNN
	1    2600 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5E129A32
P 2300 3800
F 0 "R2" V 2093 3800 50  0000 C CNN
F 1 "270" V 2184 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2230 3800 50  0001 C CNN
F 3 "~" H 2300 3800 50  0001 C CNN
	1    2300 3800
	0    1    1    0   
$EndComp
$Comp
L Device:LED D3
U 1 1 5E12C08D
P 2600 4200
F 0 "D3" H 2593 3945 50  0000 C CNN
F 1 "LED" H 2593 4036 50  0000 C CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 2600 4200 50  0001 C CNN
F 3 "~" H 2600 4200 50  0001 C CNN
	1    2600 4200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5E12C093
P 2300 4200
F 0 "R3" V 2093 4200 50  0000 C CNN
F 1 "270" V 2184 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2230 4200 50  0001 C CNN
F 3 "~" H 2300 4200 50  0001 C CNN
	1    2300 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 3400 2850 3400
Wire Wire Line
	2850 3400 2850 3800
Wire Wire Line
	2750 4200 2850 4200
Wire Wire Line
	2750 3800 2850 3800
Connection ~ 2850 3800
$Comp
L power:GND #PWR0108
U 1 1 5E133B6A
P 2850 4200
F 0 "#PWR0108" H 2850 3950 50  0001 C CNN
F 1 "GND" H 2855 4027 50  0000 C CNN
F 2 "" H 2850 4200 50  0001 C CNN
F 3 "" H 2850 4200 50  0001 C CNN
	1    2850 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5E141314
P 8800 4150
F 0 "J2" H 8718 3825 50  0000 C CNN
F 1 "Conn_01x02" H 8718 3916 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 8800 4150 50  0001 C CNN
F 3 "~" H 8800 4150 50  0001 C CNN
	1    8800 4150
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5E14130E
P 8800 4600
F 0 "J3" H 8718 4275 50  0000 C CNN
F 1 "Conn_01x02" H 8718 4366 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 8800 4600 50  0001 C CNN
F 3 "~" H 8800 4600 50  0001 C CNN
	1    8800 4600
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5E140488
P 8800 5050
F 0 "J4" H 8718 4725 50  0000 C CNN
F 1 "Conn_01x02" H 8718 4816 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 8800 5050 50  0001 C CNN
F 3 "~" H 8800 5050 50  0001 C CNN
	1    8800 5050
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5E13FB1D
P 8800 5500
F 0 "J5" H 8718 5175 50  0000 C CNN
F 1 "Conn_01x02" H 8718 5266 50  0000 C CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 8800 5500 50  0001 C CNN
F 3 "~" H 8800 5500 50  0001 C CNN
	1    8800 5500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9000 4150 9050 4150
Wire Wire Line
	9000 4600 9050 4600
Wire Wire Line
	9050 4600 9050 4150
Wire Wire Line
	9000 5500 9050 5500
Wire Wire Line
	9050 5500 9050 5050
Connection ~ 9050 4600
Wire Wire Line
	9000 5050 9050 5050
Connection ~ 9050 5050
Wire Wire Line
	9050 5050 9050 4600
$Comp
L Switch:SW_SPST SW1
U 1 1 5E15D0F6
P 3200 1700
F 0 "SW1" H 3200 1935 50  0000 C CNN
F 1 "SW_SPST" H 3200 1844 50  0000 C CNN
F 2 "fan4:KCD1-116B-2P" H 3200 1700 50  0001 C CNN
F 3 "~" H 3200 1700 50  0001 C CNN
	1    3200 1700
	1    0    0    -1  
$EndComp
Text Label 2150 3400 2    50   ~ 0
LED0
Text Label 2150 3800 2    50   ~ 0
LED1
Text Label 2150 4200 2    50   ~ 0
LED2
Text Label 5850 3850 0    50   ~ 0
LED0
Text Label 5850 3950 0    50   ~ 0
LED1
Text Label 5850 4050 0    50   ~ 0
LED2
Text Label 5850 4150 0    50   ~ 0
SPEED
Text Label 2600 5100 0    50   ~ 0
SPEED
$Comp
L power:+5V #PWR0109
U 1 1 5E122B60
P 2400 4950
F 0 "#PWR0109" H 2400 4800 50  0001 C CNN
F 1 "+5V" H 2415 5123 50  0000 C CNN
F 2 "" H 2400 4950 50  0001 C CNN
F 3 "" H 2400 4950 50  0001 C CNN
	1    2400 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5E12325F
P 2400 5250
F 0 "#PWR0110" H 2400 5000 50  0001 C CNN
F 1 "GND" H 2405 5077 50  0000 C CNN
F 2 "" H 2400 5250 50  0001 C CNN
F 3 "" H 2400 5250 50  0001 C CNN
	1    2400 5250
	1    0    0    -1  
$EndComp
Text Notes 1800 5500 1    50   ~ 0
speed setting for fan
Text Notes 5600 4850 0    50   ~ 0
PB2-PB5 have ADC
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20PU U1
U 1 1 5E1261D4
P 5250 4150
F 0 "U1" H 4720 4196 50  0000 R CNN
F 1 "ATtiny85-20PU" H 4720 4105 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 5250 4150 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 5250 4150 50  0001 C CNN
	1    5250 4150
	1    0    0    -1  
$EndComp
$Comp
L fan4-rescue:AP1013-fan4 U3
U 1 1 5E1297BC
P 8950 2050
F 0 "U3" H 8550 2650 50  0000 C CNN
F 1 "AP1013" H 8650 2550 50  0000 C CNN
F 2 "fan4:QFN-16_EP_3x3_Pitch0.5mm" H 8550 1600 50  0001 C CNN
F 3 "" H 8550 1600 50  0001 C CNN
	1    8950 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C CVC1
U 1 1 5E12B225
P 8950 3250
F 0 "CVC1" H 9065 3296 50  0000 L CNN
F 1 "1u" H 9065 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8988 3100 50  0001 C CNN
F 3 "~" H 8950 3250 50  0001 C CNN
	1    8950 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C CVIO1
U 1 1 5E12BC00
P 9350 3250
F 0 "CVIO1" H 9465 3296 50  0000 L CNN
F 1 "1u" H 9465 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9388 3100 50  0001 C CNN
F 3 "~" H 9350 3250 50  0001 C CNN
	1    9350 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:C CVM1
U 1 1 5E12BC52
P 9750 3250
F 0 "CVM1" H 9865 3296 50  0000 L CNN
F 1 "1u" H 9865 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9788 3100 50  0001 C CNN
F 3 "~" H 9750 3250 50  0001 C CNN
	1    9750 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5E12C2E0
P 8950 3400
F 0 "#PWR0111" H 8950 3150 50  0001 C CNN
F 1 "GND" H 8955 3227 50  0000 C CNN
F 2 "" H 8950 3400 50  0001 C CNN
F 3 "" H 8950 3400 50  0001 C CNN
	1    8950 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5E12C393
P 9350 3400
F 0 "#PWR0112" H 9350 3150 50  0001 C CNN
F 1 "GND" H 9355 3227 50  0000 C CNN
F 2 "" H 9350 3400 50  0001 C CNN
F 3 "" H 9350 3400 50  0001 C CNN
	1    9350 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5E12C3CE
P 9750 3400
F 0 "#PWR0113" H 9750 3150 50  0001 C CNN
F 1 "GND" H 9755 3227 50  0000 C CNN
F 2 "" H 9750 3400 50  0001 C CNN
F 3 "" H 9750 3400 50  0001 C CNN
	1    9750 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5E12CBEB
P 9550 1550
F 0 "R6" V 9343 1550 50  0000 C CNN
F 1 "100k" V 9434 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 9480 1550 50  0001 C CNN
F 3 "~" H 9550 1550 50  0001 C CNN
	1    9550 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5E12DA26
P 8350 1550
F 0 "R5" V 8143 1550 50  0000 C CNN
F 1 "100k" V 8234 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8280 1550 50  0001 C CNN
F 3 "~" H 8350 1550 50  0001 C CNN
	1    8350 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 1800 9550 1800
Wire Wire Line
	8350 1700 8350 1800
Wire Wire Line
	8350 1800 8450 1800
Wire Wire Line
	9550 1700 9550 1800
$Comp
L power:+5V #PWR0114
U 1 1 5E130A36
P 8350 1400
F 0 "#PWR0114" H 8350 1250 50  0001 C CNN
F 1 "+5V" H 8350 1600 50  0000 C CNN
F 2 "" H 8350 1400 50  0001 C CNN
F 3 "" H 8350 1400 50  0001 C CNN
	1    8350 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0115
U 1 1 5E130B30
P 9550 1400
F 0 "#PWR0115" H 9550 1250 50  0001 C CNN
F 1 "+5V" H 9550 1600 50  0000 C CNN
F 2 "" H 9550 1400 50  0001 C CNN
F 3 "" H 9550 1400 50  0001 C CNN
	1    9550 1400
	1    0    0    -1  
$EndComp
Text Label 8450 2000 2    50   ~ 0
VG
Text Label 8900 1500 1    50   ~ 0
VC
Text Label 9000 1500 1    50   ~ 0
VIO
Text Label 9100 1500 1    50   ~ 0
VM
Text Label 8950 3100 2    50   ~ 0
VC
Text Label 9350 3100 2    50   ~ 0
VIO
Text Label 9750 3100 2    50   ~ 0
VM
Wire Wire Line
	8900 1500 8900 1450
Wire Wire Line
	8900 1450 9000 1450
Wire Wire Line
	9000 1500 9000 1450
$Comp
L power:+5V #PWR0116
U 1 1 5E132BBC
P 8900 1400
F 0 "#PWR0116" H 8900 1250 50  0001 C CNN
F 1 "+5V" H 8900 1600 50  0000 C CNN
F 2 "" H 8900 1400 50  0001 C CNN
F 3 "" H 8900 1400 50  0001 C CNN
	1    8900 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 1450 8900 1400
Connection ~ 8900 1450
$Comp
L power:+12V #PWR0117
U 1 1 5E1342D6
P 9100 1400
F 0 "#PWR0117" H 9100 1250 50  0001 C CNN
F 1 "+12V" H 9100 1600 50  0000 C CNN
F 2 "" H 9100 1400 50  0001 C CNN
F 3 "" H 9100 1400 50  0001 C CNN
	1    9100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1400 9100 1500
Text Label 8450 2100 2    50   ~ 0
CH
Text Label 8450 2200 2    50   ~ 0
CL
$Comp
L power:GND #PWR0118
U 1 1 5E137706
P 8950 2650
F 0 "#PWR0118" H 8950 2400 50  0001 C CNN
F 1 "GND" H 8955 2477 50  0000 C CNN
F 2 "" H 8950 2650 50  0001 C CNN
F 3 "" H 8950 2650 50  0001 C CNN
	1    8950 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2600 8900 2650
Wire Wire Line
	8900 2650 8950 2650
Wire Wire Line
	9000 2650 9000 2600
Connection ~ 8950 2650
Wire Wire Line
	8950 2650 9000 2650
Text Label 9450 1900 0    50   ~ 0
INB
Text Label 9450 2000 0    50   ~ 0
INA
Text Label 9450 2100 0    50   ~ 0
OUTB
Text Label 9450 2200 0    50   ~ 0
OUTA
Connection ~ 2850 4200
Wire Wire Line
	2850 3800 2850 4200
Text Label 5850 4250 0    50   ~ 0
INB
Text Label 5850 4350 0    50   ~ 0
INA
$Comp
L Device:C CVG1
U 1 1 5E13B783
P 8550 3250
F 0 "CVG1" H 8665 3296 50  0000 L CNN
F 1 "0.1u" H 8665 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8588 3100 50  0001 C CNN
F 3 "~" H 8550 3250 50  0001 C CNN
	1    8550 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E13B78A
P 8550 3400
F 0 "#PWR0119" H 8550 3150 50  0001 C CNN
F 1 "GND" H 8555 3227 50  0000 C CNN
F 2 "" H 8550 3400 50  0001 C CNN
F 3 "" H 8550 3400 50  0001 C CNN
	1    8550 3400
	1    0    0    -1  
$EndComp
Text Label 8550 3100 2    50   ~ 0
VG
$Comp
L Device:C CHL1
U 1 1 5E13CCB4
P 8100 3250
F 0 "CHL1" H 8215 3296 50  0000 L CNN
F 1 "0.1u" H 8215 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8138 3100 50  0001 C CNN
F 3 "~" H 8100 3250 50  0001 C CNN
	1    8100 3250
	1    0    0    -1  
$EndComp
Text Label 8100 3100 2    50   ~ 0
CH
Text Label 8100 3400 2    50   ~ 0
CL
Text Notes 1800 3950 1    50   ~ 0
indicator LEDs
Text Notes 2200 1200 0    50   ~ 0
power
Wire Notes Line
	2200 1250 2200 2500
Wire Notes Line
	2200 2500 6200 2500
Wire Notes Line
	6200 2500 6200 1250
Wire Notes Line
	6200 1250 2200 1250
Text Notes 1650 2950 0    50   ~ 0
HMI
Wire Notes Line
	3050 3000 3050 5850
Wire Notes Line
	3050 5850 1650 5850
Wire Notes Line
	1650 5850 1650 3000
Wire Notes Line
	1650 3000 3050 3000
Text Label 9050 4150 0    50   ~ 0
OUTA
Wire Wire Line
	9000 4250 9100 4250
Wire Wire Line
	9100 4250 9100 4700
Wire Wire Line
	9100 5600 9000 5600
Wire Wire Line
	9000 4700 9100 4700
Connection ~ 9100 4700
Wire Wire Line
	9000 5150 9100 5150
Wire Wire Line
	9100 4700 9100 5150
Connection ~ 9100 5150
Wire Wire Line
	9100 5150 9100 5600
Text Label 9100 4250 0    50   ~ 0
OUTB
Wire Notes Line
	7700 1000 7700 6050
Wire Notes Line
	7700 6050 10250 6050
Wire Notes Line
	10250 6050 10250 1000
Wire Notes Line
	10250 1000 7700 1000
Text Notes 7700 950  0    50   ~ 0
motor drive
Text Notes 8300 5100 1    50   ~ 0
fan loads
Text Notes 1950 6000 0    50   ~ 0
not sure if pot sym good
Text Notes 1950 6100 0    50   ~ 0
pot setup so voltage range spans as much of 0 - VC as possible
Text Notes 1950 6200 0    50   ~ 0
made assume that 10k is good current limit
$Comp
L Device:C C3
U 1 1 5E12D6E2
P 3750 4200
F 0 "C3" H 3865 4246 50  0000 L CNN
F 1 "0.1u" H 3865 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3788 4050 50  0001 C CNN
F 3 "~" H 3750 4200 50  0001 C CNN
	1    3750 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5E12E2B3
P 3750 4050
F 0 "#PWR01" H 3750 3900 50  0001 C CNN
F 1 "+5V" H 3765 4223 50  0000 C CNN
F 2 "" H 3750 4050 50  0001 C CNN
F 3 "" H 3750 4050 50  0001 C CNN
	1    3750 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5E12E2F4
P 3750 4350
F 0 "#PWR02" H 3750 4100 50  0001 C CNN
F 1 "GND" H 3755 4177 50  0000 C CNN
F 2 "" H 3750 4350 50  0001 C CNN
F 3 "" H 3750 4350 50  0001 C CNN
	1    3750 4350
	1    0    0    -1  
$EndComp
Text Notes 3650 5300 0    50   ~ 0
note that 0.1uF is widely accepted bypass val
$Comp
L Connector:Barrel_Jack_Switch J1
U 1 1 5E13F9CA
P 2600 1800
F 0 "J1" H 2657 2117 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 2657 2026 50  0000 C CNN
F 2 "fan4:54-00166" H 2650 1760 50  0001 C CNN
F 3 "~" H 2650 1760 50  0001 C CNN
	1    2600 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1800 2950 1800
Wire Wire Line
	2950 1800 2950 1900
Connection ~ 2950 1900
Wire Wire Line
	2950 1900 3000 1900
$Comp
L Device:R_POT RV1
U 1 1 5E158702
P 2400 5100
F 0 "RV1" H 2331 5146 50  0000 R CNN
F 1 "10k" H 2331 5055 50  0000 R CNN
F 2 "fan4:P120PK" H 2400 5100 50  0001 C CNN
F 3 "~" H 2400 5100 50  0001 C CNN
	1    2400 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 5100 2600 5100
Text Label 2950 1700 0    50   ~ 0
V_RAW
$EndSCHEMATC
